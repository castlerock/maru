# Maru (&#x25cb;)

This repository is based on the original tar-ball containing Maru v2.4, as
found [here](http://piumarta.com/software/maru/). The Git tag v2.4 of this
repository marks the original set of files, with the exception that I removed
the execute bit, which seems to have been erronously set, from the following
files: buffer.c, chartab.h, emit.l, gc.c, gc.h, main.c, Makefile, model.l,
README.examples, test-emit.l.

# License

I have added a LICENSE file containing the license text from the
original Maru [web site](http://piumarta.com/software/maru) as of
2016-10-28. The web site states that Maru is distributed under the MIT
license. However, although close, the text is not identical with the
current MIT license.
